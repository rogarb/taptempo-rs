use std::io;
use std::time;

fn print_bpm(time: &time::Instant) {
    println!("{} BPM", 60.0 / time.elapsed().as_secs_f64());
}

fn print_usage() {
    println!("TapTempo: press <Enter> on every beat. Press <q> followed by <Enter> to quit.");
}

fn main() {
    let mut input = String::new();
    let mut time = None;
    print_usage();
    loop {
        match io::stdin().read_line(&mut input) {
            Ok(1) => {
                if let Some(time) = time {
                    print_bpm(&time);
                }
                time = Some(time::Instant::now());
            }
            Ok(_) => {
                if input == "q\n" {
                    println!("Exiting...");
                    break;
                } else {
                    print_usage();
                }
            }
            Err(e) => {
                println!("Error: {e}");
            }
        }
        input.clear()
    }
}
